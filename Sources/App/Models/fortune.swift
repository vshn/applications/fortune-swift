import Vapor

struct Fortune: Encodable, Content {
    var message: String
    var number: Int
    var hostname: String
    var version: String
}
