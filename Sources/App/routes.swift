import Vapor

func execute(command: String) throws -> String {
    let task = Process()
    task.executableURL = URL(fileURLWithPath: command)
    let outputPipe = Pipe()
    task.standardOutput = outputPipe
    try task.run()
    let outputData = outputPipe.fileHandleForReading.readDataToEndOfFile()
    return String(decoding: outputData, as: UTF8.self)
}

func makeData() throws -> Fortune {
    let fortune = try execute(command: "/usr/games/fortune")
    let hostname = try execute(command: "/usr/bin/hostname")
    let number = Int.random(in: 1...1000)
    let data = Fortune(message: fortune, number: number, hostname: hostname, version: "1.2-swift")
    return data
}

func respondJson(_ data: Fortune) throws -> Response {
    var headers = HTTPHeaders()
    let encoder = JSONEncoder()
    let json = try encoder.encode(data)
    headers.add(name: .contentType, value: "application/json")
    return Response(status: .ok, headers: headers, body: .init(data: json))
}

func respondText(_ data: Fortune) throws -> Response {
    var headers = HTTPHeaders()
    let message = "Fortune \(data.version) cookie of the day #\(data.number):\n\n\(data.message)"
    headers.add(name: .contentType, value: "text/plain")
    return Response(status: .ok, headers: headers, body: .init(string: message))
}

// tag::router[]
func routes(_ app: Application) throws {
    app.get { req -> EventLoopFuture<Response> in
        let data = try makeData()
        let accept = req.headers["accept"].first ?? "text/html"
        if accept == "application/json" {
            return req.eventLoop.makeSucceededFuture(try respondJson(data))
        }
        else if accept == "text/plain" {
            return req.eventLoop.makeSucceededFuture(try respondText(data))
        }
        return req.view.render("index", data).map { view in
            var headers = HTTPHeaders()
            headers.add(name: .contentType, value: "text/html")
            let str = String(decoding: view.data.readableBytesView, as: UTF8.self)
            return Response(status: .ok, headers: headers, body: .init(string: str))
        }
    }
}
// end::router[]
